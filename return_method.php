<p class="section-heading">Return Policy</p>
	<p>TemuTemu mengutamakan kepuasan pelanggan kami. Untuk pengembalian barang yang tidak berfungsi, 
		harap menghubungi kami melalui email atau nomor telepon sambil menyertakan informasi mengenai 
		detail order dan descripsi masalah yang dialami.</p>
	<p>Kami tidak bertanggung jawab jika terjadi kerusakan dalam waktu pengiriman. </p>