<?php
if (session_id() == "")
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon/">

  <title>TemuTemu - Enable Innovations</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <?php
    $_SESSION['curr-page']='toko';
    include "header.php";
    ?>


    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>
      <?php
      include 'sidebar.html';
      ?>

      <!-- Right main content -->
      <div class="col-md-10 col-sm-9 col-xs-12" >



        <!-- Main Page Cover Carousel -->
        <div id="cover-carousel" class="carousel slide carousel-cover" data-rise="carousel">
         <!-- Indicators -->
         <ol class="carousel-indicators">
          <li data-target="#cover-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#cover-carousel" data-slide-to="1"></li>
          <li data-target="#cover-carousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="cover-pic" src="/img/carousel-pic1.jpg" alt="...">
            <div class="carousel-caption">
              <h3>Jakarta</h3>
              <p>This is Main Picture 1</p>
            </div>
          </div>
          <div class="item">
            <img class="cover-pic" src="/img/eeb.jpg" alt="...">
            <div class="carousel-caption">
              <h3>UW EE Building</h3>
              <p>This is Main Picture 2</p>
            </div>
          </div>
          <div class="item">
            <img class="cover-pic" src="/img/eeb.jpg" alt="...">
            <div class="carousel-caption">
              <h3>UW EE Building</h3>
              <p>This is Main Picture 3</p>
            </div>
          </div>
        </div> <!-- /Wrapper for slides-->
        <!-- Controls -->
        <a class="left carousel-control" href="#cover-carousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#cover-carousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div> <!-- /Main Page Cover Carousel -->
      
      <!-- Temutemu Banner -->
      <div class="temutemu-banner"> 
        <h1>" Selamat Datang di TemuTemu.com! "</h1>
        <h3>Tempat memulai kreasi anda</h3>
      </div><!-- /Temutemu Banner -->

      <br>

      <!-- 1st Row -->
      <p class="section-heading">Product Terbaru</p>
      <div class="row">
      </div> <!-- /1st Row -->

      <!-- 2nd Row -->
      <p class="section-heading">Tutorial Terbaru</p>
      <div class="row">
      </div> <!-- /2nd Row -->

      <!-- 2nd Row -->
      <p class="section-heading">News</p>
      <div class="row">
      </div> <!-- /2nd Row -->

      <br>
      <br>
    </div> <!-- / Right main content -->

    <br>
    <br>
  </div> <!-- / Main Container -->

  <?php
  include 'footer.html';
  ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
