<?php
/*
Function for getting specific product list 
based on the product's category
*/
function getProductsList($qType){
  try {
    // Connecting to database
    $db = connectToDB();

    // Connecting to database
    //$db = new PDO ("mysql:host=temutemu.com;dbname=temd4647_tokodb","temd4647_edwinp3","Pipoputih_13");

    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // determine the type of query here
    switch ($qType) {
      case 1: // dev board
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 1");
      break;
      case 2: // sensor
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 2");
      break;
      case 3: // breakout boards
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 3");
      break;
      case 4: // mechanical
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 4");
      break;
      case 5: // ICs
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 5");
      break;
      case 6: // Prototyping tools
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 6");
      break;
      case 7: // MCUs
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 7");
      break;
      case 8: // Components
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Category_ID = 8");
      break;
      case 9: // Popular Products
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.P_Hits > 20");
      break;
      default:
      $rows = $db->query("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Pictures.P_Thumb FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID");
      break;
    }

    $row_cnt = $rows->rowCount();
    if($row_cnt > 0){
      ?>
      <ul>
        <?php
      // print out the product list
        foreach ($rows as $row) {
          $link = sprintf("/product/%d", $row["ID"]);
          $p_img = sprintf("/img/product_pics/%s", $row["P_Thumb"]);
          $p_name = $row["P_Name"];
          $p_price = $row["P_Price"];
          $p_price = number_format( $p_price, 2, ',', '.');

          ?>
          <li class="tile">
            <a href=<?= $link ?>>
              <img class="p_thumb" src=<?= $p_img ?> />
              <p class="p_name"><?= $p_name ?></p>
              <p class="p_price">Rp. <?= $p_price ?></p>
            </a>
          </li>
          <?php
        }

        ?>
      </ul>
      <?php
    }else if($row_cnt <= 0 ){
      ?>
      <p>Maaf, product yang anda cari tidak tersedia</p>
      <?php
    }

  } catch (PDOException $ex) {
    ?>
    <p>Sorry, a database error occurred. Please try again later.</p>
    <p>(Error details: <?= $ex->getMessage() ?>)</p>
    <?php
  }

  // disconnect from database
  disconnectFromDB($db);
}

/*
Function to query and display current cart
Return the total cost in the shopping cart
*/
function displayCart(){

  if($_SESSION['cart']){ // show cart if it's not empty
  try {
    // Connecting to database
    $db = connectToDB();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach($_SESSION['cart'] as $product_id => $quantity) { 
      $sql = sprintf("SELECT P_Name, P_Price FROM Product_Main WHERE ID = %d;",
        $product_id);
      $results = $db->query($sql);

      foreach ($results as $result) {
        $name = $result["P_Name"];
        $price = $result["P_Price"];
        $p_price = number_format( $price, 2, ',', '.');

        $product_cost = $price * $quantity; // calculate product cost
        $p_product_cost = number_format( $product_cost, 2, ',', '.');
        $total = $total + $product_cost; // calculate total cost
      }

      $del_id = sprintf("/cart.php?id=%d&action=remove&quantity=0", $product_id);// set target delete id
      ?>
      <tr>
        <td><?= $name ?></td>
        <td>
          <form class="form-cart" action="/cart.php" method="get">
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="id" value=<?= $product_id ?> />
            <input type="text" class="form-control quantity-box-cart" name="quantity" value=<?= $quantity ?> size="5000" required="required" pattern="[0-9]+" title="Input type is integer only [0-9]"/>
            <button type="submit" class="refresh-btn"><span class="glyphicon glyphicon-refresh"></span></button>
          </form>
          <a href=<?= $del_id ?>><span class="glyphicon glyphicon-trash"></span></a>
        </td>
        <td>Rp <?= $p_price ?></td>
        <td>Rp <?= $p_product_cost ?></td>
      </tr>
      <?php
      
    }
  } catch (PDOException $ex) {
    ?>
    <p>Sorry, a database error occurred. Please try again later.</p>
    <p>(Error details: <?= $ex->getMessage() ?>)</p>
    <?php
  }
}else{
  ?>
  <tr>
    <td>Your cart is empty</td>
  </tr>
  <?php
}

   // disconnect from database
disconnectFromDB($db);

if($total == NULL){
  $total = 0;
}

return $total;
}

/*
Function to query and display final cart
Return the total cost in the shopping cart
*/
function displayFinalCart(){
  try {
    // Connecting to database
    $db = connectToDB();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach($_SESSION['cart'] as $product_id => $quantity) { 
      $sql = sprintf("SELECT P_Name, P_Price FROM Product_Main WHERE ID = %d;",
        $product_id);
      $results = $db->query($sql);

      foreach ($results as $result) {
        $name = $result["P_Name"];
        $price = $result["P_Price"];
        $p_price = number_format( $price, 2, ',', '.');

        $product_cost = $price * $quantity; // calculate product cost
        $p_product_cost = number_format( $product_cost, 2, ',', '.');
        $total = $total + $product_cost; // calculate total cost
      }

      $del_id = sprintf("/cart.php?id=%d&action=remove&quantity=0", $product_id);// set target delete id
      ?>
      <tr>
        <td><?= $name ?></td>
        <td><?= $quantity ?></td>
        <td>Rp <?= $p_price ?></td>
        <td>Rp <?= $p_product_cost ?></td>
      </tr>

      <?php
    }

  } catch (PDOException $ex) {
    ?>
    <p>Sorry, a database error occurred. Please try again later.</p>
    <p>(Error details: <?= $ex->getMessage() ?>)</p>
    <?php
  }

   // disconnect from database
  disconnectFromDB($db);

  if($total == NULL){
    $total = 0;
  }

  return $total;
}

/*
Function to query and display final cart
Return the total cost in the shopping cart
*/
function getFinalCartMsg(){
  $cart_html = "
  <table>
     <tr>
      <th>Item Name</th><th>Quantity</th><th>Product Price</th><th>Total Cost</th>
    </tr>
  ";
 
  
  try {
    // Connecting to database
    $db = connectToDB();
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach($_SESSION['cart'] as $product_id => $quantity) { 
      $sql = sprintf("SELECT P_Name, P_Price FROM Product_Main WHERE ID = %d;",
        $product_id);
      $results = $db->query($sql);

      foreach ($results as $result) {
        $name = $result["P_Name"];
        $price = $result["P_Price"];
        $p_price = number_format( $price, 2, ',', '.');

        $product_cost = $price * $quantity; // calculate product cost
        $p_product_cost = number_format( $product_cost, 2, ',', '.');
        $total = $total + $product_cost; // calculate total cost
        $p_total = number_format( $total, 2, ',', '.');
      }

      $cart_html .= "
      <tr>
      <td>". $name ."</td><td>". $quantity ."</td><td>Rp ". $p_price ."</td><td>Rp ". $p_product_cost ."</td>
      </tr>
      ";
    }

    $cart_html .= "</table>";
    $cart_html .="<p><b>Total: </b>". $p_total ."<p>";

  } catch (PDOException $ex) {
    ?>
    <p>Sorry, a database error occurred. Please try again later.</p>
    <p>(Error details: <?= $ex->getMessage() ?>)</p>
    <?php
  }

   // disconnect from database
  disconnectFromDB($db);

  return $cart_html;
}

/*
Function to connect to Database
*/
function connectToDB(){
  /*
  $hostname = "localhost"; // define host name
  $dbname = "Estore"; // define data base name
  $username = "root"; // define username
  $pw = "root"; // define password
  */  

  $hostname = "localhost"; // define host name
  $dbname = "temd4647_tokodb"; // define data base name
  $username = "temd4647_edwinp3"; // define username
  $pw = "Tempo_12345"; // define password

  $sql = sprintf("mysql:host=%s;dbname=%s", $hostname, $dbname);

  // Connecting to database
  return new PDO ($sql,$username,$pw);
}

/*
Function to disconnect from Database
*/
function disconnectFromDB($db){
  // disconnect from database
  if($db != NULL){
    $db = NULL;
  }
}

?>
