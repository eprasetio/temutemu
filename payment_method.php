<p class="section-heading">Metode Pembayaran</p>
          <p>Untuk saat ini, pembayaran hanya dapat dilakukan melalui bank transfer ke account berikut:</p>
          <h5>Bank BCA</h5>
          <h5>Account # 4900351513</h5>
          <h5>Edwin Prasetio</h5>
          <h5>KCP Taman Permata Buana</h5>

          <br>

          <p>Harap menyelesaikan pembayaran paling lambat 24 jam setelah mengirimkan pemesanan. Jika pembayaran belum diterima dalam kurun waktu 24 jam setelah pemesanan, maka pemesanan anda akan kami batalkan</p>

          <p>Setelah melalukan pembayaran, harap menkonfirmasi pembayaran anda melalui SMS ke nomor berikut: 123 45678 dengan melampirkan keterangan dibawah</p>
          <h5>Order ID:</h5> 
          <h5>Nama:</h5>
          <h5>Bank:</h5>
          <h5>Account Holder:</h5>

          <br>
          
          <p>Terima Kasih</p>