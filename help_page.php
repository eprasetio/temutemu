<?php  
if (session_id() == "")
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon/">

  <title>TemuTemu - Enable Innovations</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>
    <?php
    $_SESSION['curr-page']='toko';
    include "header.php";
    ?>


    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>

      <!-- Right main content -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="row">
          
        <?php
          $ht = $_GET["hType"]; // get the  product category type
          
          switch ($ht) {
            case '0':
              include 'order_method.php';
              break;
            case '1':
              include 'shipping_method.php';
              break;
            case '2':
              include 'payment_method.php';
              break;
            case '3':
              include 'return_method.php';
              break;
            default:
              include 'payment_method.php';
              break;
          }
        ?>

        </div>
      </div> <!-- / Right main content -->

    <br>
    <br>
  </div> <!-- / Main Container -->

  <?php
  include 'footer.html';
  ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
