<p class="section-heading">Cara Mengorder Barang</p>

<img class="help-pics" src="/img/help_pics/order_1.png"></img>
<p><h5>Step 1: Masukan barang ke keranjang belanja elektronik</h5>
Untuk memulai melakukan pemesanan barang, masuk ke halaman barang yang akan di beli seperti ditampilkan di gambar di atas. 
Masukan jumlah barang yang akan di beli, lalu klik tombol add to cart untuk menambahkan barang yang akan di pesan ke dalam keranjang.
</p>

<img class="help-pics" src="/img/help_pics/order_2.png"></img>
<p><h5>Step 2: Review daftar belanja anda</h5>
Untuk melihat daftar belanja anda di keranjang elektronik, klik icon cart di pojok kanan atas.
Jika ingin menambah atau mengurangi jumlah barang yang anda pesan, masukan jumlah barang yang baru di text box yang terletak di kolom Qty.
Setelah selesai, klik icon refresh di sebelah text box tersebut. Untuk membuang barang dari keranjang belanja, klik icon trash can di samping kanan icon refresh. 
Untuk mengosongkan keranjang belanja anda, klik tulisan empty shopping cart di bawah kiri.<br>Jika anda sudah siap untuk men-submit daftar belanja anda,
klik tombol checkout di kanan bawah.
</p>

<img class="help-pics" src="/img/help_pics/order_3.png"></img>
<p><h5>Step 3: Memasukan keterangan pemesan</h5>
Di halaman berikutnya, silahkan mengisi form keterangan pemesan. Kami akan menggunakan informasi tersebut untuk
pengiriman barang yang akan dipesan. Setelah selesai, klik tombol Review order.
</p>

<img class="help-pics" src="/img/help_pics/order_4.png"></img>
<p><h5>Step 3: Review daftar belanja dan keterangan pemesan sebelum men-submit order</h5>
Pada halaman ini, anda dapat mereview daftar pesanan anda beserta keterangan pemesan untuk terakhir kalinya sebelum men-submit pesanan anda ke data base kami.
Jika anda sudah siap men-submit daftar pesanan anda, silahkan menklik tombol Submit Order. Setelah mensubmit pesanan anda, silahkan melakukan pembayaran sebesar total jumlah pesanan anda dengan metode yang dijelasan di <a style="color:#fe7600;" href="/help/2">sini</a>.
Pesanan anda akan kami simpan dalam waktu 24 jam untuk menunggu konfirmasi pembayaran anda. Jika dalam waktu 24 jam kami belum menerima konfirmasi pembayaran anda, pesanan anda akan kami hapus dalam database kami.  
</p>