<?php  
if (session_id() == "")
  session_start();
?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.ico">

  <title>TemuTemu Shipping Page</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php
    $_SESSION['curr-page']='cart';
    include 'header.php';
    ?>

    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>
      <?php
      include 'sidebar.html';
      ?>

      <!-- Right main content -->
      <div class="col-md-10 col-sm-9 col-xs-12" >

        <!-- 1st Row -->
        <p class="section-heading">Shipping Information</p>
        <div class="row shipping-info-container">
          <p>Please fill in your shipping information below</p>
          <form action="/ship.php" method="get">
            <div class="col-md-2 col-xs-12" >

              <label>First Name:</label>
              <input type="text" class="shipping-info-box" value="" name="firstname" required="required">

              <label>Last Name:</label>
              <input type="text" class="shipping-info-box" value="" name="lastname" required="required">

              <label>Address 1:</label>
              <input type="text" class="shipping-info-box" value="" name="address_1" required="required">

              <label>Address 2:</label>
              <input type="text" class="shipping-info-box" value="" name="address_2">

              <label>City:</label>
              <input type="text" class="shipping-info-box" value="" name="city" required="required">

              <label>Province:</label>
              <input type="text" class="shipping-info-box" value="" name="province" required="required">

              <label>Post Code:</label>
              <input type="text" class="shipping-info-box" value="" name="postcode" required="required" pattern="[0-9]+" title="Please input number only [0-9]">

              <label>Phone:</label>
              <input type="text" class="shipping-info-box" value="" name="phone" required="required" pattern="[0-9]+" title="Please input number only [0-9]">

              <label>Email:</label>
              <input type="text" class="shipping-info-box" value="" name="email">

              <label>Blackberry Pin:</label>
              <input type="text" class="shipping-info-box" value="" name="bb_pin">

            </div>
            
          </div> <!-- /1st Row -->
          <!-- 2nd Row -->
          <div class="row">
            <br>
            <br>
            <a href="/cart" class="estore-btn">Back</a>
            <input type="submit" class="estore-btn pull-right" value="Review Order">
          </form>
        </div> <!-- /2nd Row -->

        <br>
        <br>
      </div> <!-- / Right main content -->

      <br>
      <br>
    </div> <!-- / Main Container -->

    <?php
    include 'footer.html';
    ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
