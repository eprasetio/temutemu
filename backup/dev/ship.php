<?php
if (session_id() == "")
  session_start();

emptyInfoSesStr();
$info_str = NULL;

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $firstname = $_GET['firstname']; // get firstname
  $lastname = $_GET['lastname']; // get lastname
  $addr_1 = $_GET['address_1']; // get 1st address
  $addr_2 = $_GET['address_2']; // get 2nd address
  $city = $_GET['city']; // get city
  $province = $_GET['province']; // get province
  $postcode = $_GET['postcode']; // get postcode
  $phone = $_GET['phone']; // get state
  if( isset($_GET['email']) )
  	$email = $_GET['email']; // get postcode

  if( isset($_GET['bb_pin']) )
  	$bb_pin = $_GET['bb_pin']; // get BB pin
  
}

$info_str .= "First Name: " . $firstname . "\n";
$info_str .= "Last Name: " . $lastname . "\n";
$info_str .= "Address 1: " . $addr_1 . "\n";
$info_str .= "Address 2: " . $addr_2 . "\n";
$info_str .= "City: " . $city . "\n";
$info_str .= "Province: " . $province. "\n";
$info_str .= "Phone: " . $phone . "\n";
if( isset($_GET['email']) )
  $info_str .= "Email: " . $email . "\n";
if( isset($_GET['bb_pin']) )
  $info_str .= "BB Pin: " . $bb_pin . "\n";

$_SESSION['ship-info']['first-name'] = $firstname;
$_SESSION['ship-info']['last-name'] = $lastname;
$_SESSION['ship-info']['addr1'] = $addr_1;
$_SESSION['ship-info']['addr2'] = $addr_2;
$_SESSION['ship-info']['city'] = $city;
$_SESSION['ship-info']['province'] = $province;
$_SESSION['ship-info']['phone'] = $phone;
if( isset($_GET['email']) )
  $_SESSION['ship-info']['email'] = $email;
if( isset($_GET['bb_pin']) )
  $_SESSION['ship-info']['bb-pin'] = $bb_pin;

addInfoToSesStr($info_str);
$next_page = 'Location: /checkout';

header($next_page);

/*
Function to disconnect from Database
*/
function disconnectFromDB($db){
  // disconnect from database
  if($db != NULL){
    $db = NULL;
  }
}

/*
Function to add info to session string variable
*/
function addInfoToSesStr($input_str){
  // add shipping info to a session variable
  $_SESSION['ship-str'] .= $input_str; 
}

/*
Function to empty session string variable
*/
function emptyInfoSesStr(){
  unset($_SESSION['ship-str']); 
}

/*
Function to get session string variable content
*/
function getInfoSesStr(){
  return $_SESSION['ship-str']; 
}

?>