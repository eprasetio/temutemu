<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.ico">

  <title>TemuTemu - Enable Innovations</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- Landing Page Header -->
    <div id="landing-page-header">
    </div> <!-- /Landing Page Header-->

     <!-- Landing Page Strap -->
    <div id="landing-page-strap">
    </div> <!-- /Landing Page Strap -->

   <!-- Landing Page Main -->
   <div id="landing-page-main">
    <p id="landing-p1">Selamat datang di TemuTemu!<img style="width:120px;height:130px;" src="./img/favicon2.png"></p>
    <p id="landing-p1-2">Tempat membangun kreatifitas elektronika anda</p>
    <p id="landing-p2">Mohon maaf sebelumnya, saat ini kami sedang membangun 
      TemuTemu untuk anda sekalian. Jika anda tertarik untuk menerima update 
      mengenai TemuTemu dari kami, daftarkan email anda di bawah ini. </p>

      <form id="signup-form" action="/signup.php" method="get">
        <input type="text" class="signup-box" placeholder="youremail@example.com" name="usr_email" required="required">
        <input type="submit" class="signup-btn" value="Sign Up">
      </form>
    </div> <!-- /Landing Page Main -->

     <!-- Landing Page Strap -->
    <div id="landing-page-strap">
    </div> <!-- /Landing Page Strap -->

    <!-- Landing Page Footer -->
    <div id="landing-page-footer">
    </div> <!-- /Landing Page Footer -->

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
