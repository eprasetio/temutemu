<?php
if (session_id() == "")
  session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $product_id = (int) $_GET['id']; // get product ID
  $action = $_GET['action']; // get action
  $quantity = (int) $_GET['quantity']; // get product ID
}

// display error msg if product doesn't exist
if(!$product_id){
  die("Error. Product doesn't exist!");
}

switch ($action) {
  case "add":
    // add the product into the cart
  $_SESSION['cart'][$product_id] = $_SESSION['cart'][$product_id] + $quantity; 
  $prev_page = sprintf("Location: /product/%d", $product_id);
  break;
  case "update":
    // remmove the productfrom the cart completely if the product quantity = 0
  if($quantity == 0){
    unset($_SESSION['cart'][$product_id]);
  }else{
    //update the product quantity
    $_SESSION['cart'][$product_id] = $quantity; 
  }
  $prev_page = "Location: /cart";
  break;
  case "remove":
    // remmove the productfrom the cart completely if the product quantity = 0
  unset($_SESSION['cart'][$product_id]);
  $prev_page = "Location: /cart";
  break;
  case "empty":
    // empty the cart
  unset($_SESSION['cart']);
  unset($_SESSION['cart-quantity']);
  $prev_page = "Location: /cart";
  break;
  default:
  # do nothing
  break;
}

if($_SESSION['cart'] != NULL){
  countItemsInCart();
}else{
  $_SESSION["cart-quantity"] = 0;
}

header($prev_page);

function countItemsInCart(){
// count how many items in cart currently
  $total_quantity = 0;
  foreach($_SESSION['cart'] as $product_id => $quantity) { 
    $total_quantity = $total_quantity + $quantity;
  }
  $_SESSION["cart-quantity"] = $total_quantity; // put total quantity in a session
}




?>