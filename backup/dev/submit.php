<?php
if (session_id() == "")
	session_start();

require_once("query.php");

$ship_str  = $_SESSION['ship-str'];

// multiple recipients
$to  = 'edwin.prasetio3@gmail.com' . ', '; // note the comma
$to .= $_SESSION['ship-info']['email'];

// subject
$subject = 'TemuTemu Order Confirmation: 12345';

// message
$message = "
<html>
<head>
<title>TemuTemu Order Confirmation</title>
</head>
<body>
<h4>Below is your order summary.</h4>
". getShippingInfoMsg() ."
<br><br>
". getFinalCartMsg() ."
</body>
</html>
";

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'From: TemuTemu' . "\r\n";

// Mail it
mail($to, $subject, $message, $headers);

unset($_SESSION['cart']);
unset($_SESSION["cart-quantity"]);
unset($_SESSION['ship-info']);

require_once("checkout_finish_page.php");



function getShippingInfoMsg(){
	$shipMsg = "
	<p><b>Shipping info :</b></p><br>
	";

	$shipMsg .="
	<tr>
	<th>First Name: </th><td>". $_SESSION['ship-info']['first-name'] ."</td>
	</tr>
	<tr>
	<th>Last Name: </th><td>". $_SESSION['ship-info']['last-name'] ."</td>
	</tr>
	<tr>
	<th>Address 1: </th><td>". $_SESSION['ship-info']['addr1'] ."</td>
	</tr>
	<tr>
	<th>Address 2: </th><td>". $_SESSION['ship-info']['addr2'] ."</td>
	</tr>
	<tr>
	<th>City: </th><td>". $_SESSION['ship-info']['city'] ."</td>
	</tr>
	<tr>
	<th>Province: </th><td>". $_SESSION['ship-info']['province'] ."</td>
	</tr>
	<tr>
	<th>Phone: </th><td>". $_SESSION['ship-info']['phone'] ."</td>
	</tr>
	<tr>
	<th>Email: </th><td>". $_SESSION['ship-info']['email'] ."</td>
	</tr>
	<tr>
	<th>BB Pin: </th><td>". $_SESSION['ship-info']['bb-pin'] ."</td>
	</tr>
	";

	$shipMsg .= "</table>";

	return $shipMsg;
}

?>