<?php  
if (session_id() == "")
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon/">

  <title>TemuTemu - Enable Innovations</title>

  <!-- Bootstrap core CSS -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="./css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>
    <?php
    $_SESSION['curr-page']='toko';
    include "header.php";
    ?>


    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>
      <?php
      include 'sidebar.html';
      ?>

      <!-- Right main content -->
      <div class="col-md-10 col-sm-9 col-xs-12" >
        <div class="row">

          <div class="cover">
           <!-- Temutemu Banner -->
           <div class="temutemu-banner"> 
            <h1 id="welcome-msg">Selamat Datang di TemuTemu!</h1>
            <h3>Tempat membangun kreatifitas elektronika anda</h3>
          </div><!-- /Temutemu Banner -->

          <div class="cover-pic">
            <img src="./img/cover_pic.jpeg" alt="...">
          </div>
        </div>

        <!-- Step Banner -->
        <div class="step-container row"> 
          <div class="step-block dark-block col-md-3 col-sm-3 col-xs-6">
            <span class="step-block-icon glyphicon glyphicon-cloud"></span>
            <h3>Pilih Idemu</h3>
          </div>
          <div class="step-block light-block col-md-3 col-sm-3 col-xs-6">
            <span class="step-block-icon glyphicon glyphicon-hdd"></span>
            <h3>Tentukan Otaknya</h3>
          </div>
          <div class="step-block dark-block col-md-3 col-sm-3 col-xs-6">
            <span class="step-block-icon glyphicon glyphicon-th"></span>
            <h3>Design Sistemnya</h3>
          </div>
          <div class="step-block light-block col-md-3 col-sm-3 col-xs-6">
            <span class="step-block-icon glyphicon glyphicon-wrench"></span>
            <h3>Rakit idemu!</h3>
          </div>
        </div><!-- /Step Banner -->

        <!-- Persuade Message -->
        <div>
          <h1 id="persuade-msg">Ayo wujudkan idemu sekarang juga!</h1>
        </div>

        <!-- Product List Button -->
        <div class="main-page-btn center-block"> 
          <a class="estore-btn" href="/product_list/0">Lihat semua product</a>
        </div>

        <br>
        <br>
      </div>
    </div> <!-- / Right main content -->

    <br>
    <br>
  </div> <!-- / Main Container -->

  <?php
  include 'footer.html';
  ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
