<p class="section-heading">Pengiriman</p>
          <p>Begitu kami menerima order serta pembayaran anda, order anda akan kami proses dalam waktu paling lambat 24 jam.
               Setelah order selesai diproses, kami akan mengirimkan order anda dengan menggunakan jasa JNE dan kami akan mengirimkan informasi pengiriman
               ke anda. Lama pengiriman biasa memakan waktu sekitar 3 sampai 5 hari kerja. 
          </p>
          <br>
          <p><b>PS:</b>Kami tidak bertanggung jawab jika terjadi kerusakan selama masa pengiriman.</p>
