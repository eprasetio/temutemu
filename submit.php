<?php
if (session_id() == "")
	session_start();

require_once("query.php");

$shipping_info = getShippingInfoMsg();
$cart_info = getFinalCartMsg();

$order_number = logOrder($shipping_info, $cart_info);

$ship_str  = $_SESSION['ship-str'];

// multiple recipients
$to  = 'edwin.prasetio3@gmail.com' . ', '; // note the comma
$to .= $_SESSION['ship-info']['email'];

// subject
$subject = sprintf("TemuTemu Order # %d Confirmation Email", $order_number);

// message
$message = "
<html>
<head>
<title>TemuTemu Order Confirmation</title>
</head>
<body>
<h4>Below is your order summary.</h4>
". $shipping_info ."
<br><br>
". $cart_info ."
</body>
</html>
";

// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'From: TemuTemu' . "\r\n";

// Mail it
mail($to, $subject, $message, $headers);

unset($_SESSION['cart']);
unset($_SESSION["cart-quantity"]);
unset($_SESSION['ship-info']);

require_once("checkout_finish_page.php");



function getShippingInfoMsg(){
	$shipMsg = "
	<p><b>Shipping info :</b></p><br>
	";

	$shipMsg .="
	<tr>
	<th>First Name: </th><td>". $_SESSION['ship-info']['first-name'] ."</td>
	</tr>
	<tr>
	<th>Last Name: </th><td>". $_SESSION['ship-info']['last-name'] ."</td>
	</tr>
	<tr>
	<th>Address 1: </th><td>". $_SESSION['ship-info']['addr1'] ."</td>
	</tr>
	<tr>
	<th>Address 2: </th><td>". $_SESSION['ship-info']['addr2'] ."</td>
	</tr>
	<tr>
	<th>City: </th><td>". $_SESSION['ship-info']['city'] ."</td>
	</tr>
	<tr>
	<th>Province/State/Region: </th><td>". $_SESSION['ship-info']['province'] ."</td>
	</tr>
	<tr>
	<th>Zip Code: </th><td>". $_SESSION['ship-info']['postcode'] ."</td>
	</tr>
	<tr>
	<th>Country: </th><td>". $_SESSION['ship-info']['country'] ."</td>
	</tr>
	<tr>
	<th>Phone: </th><td>". $_SESSION['ship-info']['phone'] ."</td>
	</tr>
	<tr>
	<th>Email: </th><td>". $_SESSION['ship-info']['email'] ."</td>
	</tr>
	<tr>
	<th>BB Pin: </th><td>". $_SESSION['ship-info']['bb-pin'] ."</td>
	</tr>
	";

	$shipMsg .= "</table>";

	return $shipMsg;
}

function logOrder($input1, $input2){
	$order_id = 0;
  try {
    // Connecting to database
    $db = connectToDB();

    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = sprintf("INSERT INTO Order_Log (Shipping_Info, Cart_Info) VALUES ('%s', '%s')", $input1, $input2);

    $db->query($sql);

    // executing the QUERY request to MySQL server, mysql_query() returns resource ID
    $sql = "SELECT OrderID FROM Order_Log ORDER BY OrderID DESC LIMIT 1";
	$result = $db->query($sql);
	// then we use this resource ID to fetch the actual value MySQL have returned
	$row = $result->fetch(PDO::FETCH_ASSOC);
	// $row variable now holds all the data we got from MySQL, its an array, so we just output the public_id column value to the screen
	//echo "Last Order ID: " . $row['OrderID']; // you can use var_dump($row); to see the whole content of $row

	$order_id = $row['OrderID'];

  } catch (PDOException $ex) {
    ?>
    <p>Sorry, a database error occurred. Please try again later.</p>
    <p>(Error details: <?= $ex->getMessage() ?>)</p>
    <?php
  }

  // disconnect from database
  disconnectFromDB($db);

  return $order_id;
}


?>