/*
-- Query: SELECT * FROM Estore.Product_Category
LIMIT 0, 1000

-- Date: 2014-06-25 17:03
*/



CREATE TABLE `Product_Category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `P_Category` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  UNIQUE KEY `P_Category_UNIQUE` (`P_Category`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (3,'Breakout Boards');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (8,'Components');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (1,'Development Board');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (5,'Integrated Circuit');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (4,'Mechanical');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (7,'Microcontrollers');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (6,'Prototyping Tools');
INSERT INTO `Product_Category` (`ID`,`P_Category`) VALUES (2,'Sensors');

CREATE TABLE `Product_Documents` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `P_Schematic` varchar(45) DEFAULT NULL,
  `P_Datasheet` varchar(45) DEFAULT NULL,
  `P_Firmware` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Product_Main` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `P_Name` varchar(45) NOT NULL,
  `P_Doc_ID` int(11) DEFAULT NULL,
  `P_Pic_ID` int(11) DEFAULT NULL,
  `P_Tutorial_ID` int(11) DEFAULT NULL,
  `P_Price` double NOT NULL,
  `P_Description` longtext,
  `P_Stock` int(11) NOT NULL,
  `P_Category_ID` int(11) NOT NULL,
  `P_Hits` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

CREATE TABLE `Product_Pictures` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `P_Main_Pic` varchar(45) DEFAULT NULL,
  `P_Pic1` varchar(45) DEFAULT NULL,
  `P_Pic2` varchar(45) DEFAULT NULL,
  `P_Pic3` varchar(45) DEFAULT NULL,
  `P_Thumb` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `Tutorial_Main` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `T_Title` varchar(45) NOT NULL,
  `T_Inst_ID` int(11) DEFAULT NULL,
  `T_Pic_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Tutorial_Pics` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `T_Pic1` varchar(45) DEFAULT NULL,
  `T_Pic2` varchar(45) DEFAULT NULL,
  `T_Pic3` varchar(45) DEFAULT NULL,
  `T_Pic4` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Tutorial_Steps` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `T_Step1` longtext,
  `T_Step2` longtext,
  `T_Step3` longtext,
  `T_Step4` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (1,'Atmega328p Dev Board',NULL,NULL,NULL,100000,'This is AVR Dev Board',20,1,1);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (2,'AVR Programmer',NULL,NULL,NULL,200000,'This is AVR Programmer',25,1,2);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (3,'Arduino UNO',NULL,NULL,NULL,250000,'An Arduino UNO Board',15,1,20);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (4,'Accelerometer',NULL,NULL,NULL,70000,'An ACC001 Sensor',21,2,10);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (5,'Gyro',NULL,NULL,NULL,90000,'Gyrometer sensor',20,2,15);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (6,'Magnetometer',NULL,NULL,NULL,75000,'A sensor to sense magnet',5,2,12);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (7,'Alcohol Sensor',NULL,NULL,NULL,120000,'A sensor to sense alcohol level',3,2,2);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (8,'Carbon Monoxide Sensor',NULL,NULL,NULL,100000,'Module for sensing CO2 emission',7,2,4);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (9,'Light Sensor',NULL,NULL,NULL,50000,'A sensor to sense light intensity',5,2,22);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (10,'Proximity Sensor',NULL,NULL,NULL,60000,'A module to sense distance',3,2,50);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (11,'Temperature Sensor',NULL,NULL,NULL,45000,'A module to measure temperature level',8,2,3);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (12,'PIR Sensor',NULL,NULL,NULL,30000,'A distance sensor with IR light',11,2,2);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (13,'CO Sensor Breakout Board',NULL,NULL,NULL,15000,'A breakout board for CO sensor',10,3,0);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (14,'Alcohol Sensor Breakout Board',NULL,NULL,NULL,20000,'Breakout board for Alcohol Sensor',10,3,0);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (15,'Magnetometer breakout board',NULL,NULL,NULL,25000,'BB for magnetometer',9,3,2);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (16,'Gyro Breakout board',NULL,NULL,NULL,20000,'BB for gyro sensor',2,3,4);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (17,'Accelerometer Breakout Board',NULL,NULL,NULL,30500,'BB for accelerometer sensor',2,3,6);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (18,'DC Motor',NULL,NULL,NULL,45500,'A DC motor item',4,4,11);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (19,'Servos',NULL,NULL,NULL,50500,'A servo with 6 axis',2,4,40);
INSERT INTO `Product_Main` (`ID`,`P_Name`,`P_Doc_ID`,`P_Pic_ID`,`P_Tutorial_ID`,`P_Price`,`P_Description`,`P_Stock`,`P_Category_ID`,`P_Hits`) VALUES (20,'Stepper Motor',NULL,NULL,NULL,45000,'A stepper motor with 25 turns',10,4,33);




