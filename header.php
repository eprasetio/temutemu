<?php 
session_start();

$mark_toko = "";
$mark_tutorial = "";
$mark_cart = "";
$style = "color: #0d5a75;";//"background-color:#fe7600;padding: 5px;color:#fff";
$style2 = "color:#fff";
if($_SESSION['curr-page'] == 'toko'){
  $mark_toko = $style;
} else if ($_SESSION['curr-page'] == 'tutorial'){
  $mark_tutorial = $style;
} else if ($_SESSION['curr-page'] == 'cart'){
  $mark_cart = $style;
}


require_once("query.php");

$item_quantity = $_SESSION["cart-quantity"];
if($item_quantity == NULL){
  $item_quantity = 0;
}
?>
<!--<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <a href="/home"><img class="navbar-brand" src="/img/logo.png"/></a>
    </div>
    <ul class="list-inline main-nav navbar-right">
      <li style="<?= $mark_toko ?>" ><a href="/home">Toko</a></li>
      <li style="<?= $mark_tutorial ?>" ><a href="http://temutemu.blogspot.com/">Tutorial</a></li>
      <li style="<?= $mark_cart ?>" ><a href="/cart">Cart: <?= $item_quantity ?></a></li>
    </ul>
  </div>
</div>-->


<!-- Static navbar -->
<div class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <a href="/home"><img class="navbar-brand" src="/img/logo.png"/></a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav main-nav">
        <li><a style="<?= $mark_toko ?>" href="/home">Toko</a></li>
        <li><a href="http://temutemu.blogspot.com/">Tutorial</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right cart-nav">
        <li><a href="/cart"><span class="glyphicon glyphicon-shopping-cart"></span>Cart: <?= $item_quantity ?></a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div><!--/.container-fluid -->
</div>