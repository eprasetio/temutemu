<?php  
if (session_id() == "")
  session_start();

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  $product_id = (int) $_GET['id']; // get product ID
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon.ico">

  <title>TemuTemu Product Page</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <?php
    $_SESSION['curr-page']='toko';
    include "header.php";

    try {
    // Connecting to database
      $db = connectToDB();
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      //$sql = sprintf("SELECT * FROM Product_Main WHERE ID = %d;", $product_id);
      $sql = sprintf("SELECT Product_Main.ID, Product_Main.P_Name, Product_Main.P_Price, Product_Main.P_Description, Product_Main.P_Stock, Product_Pictures.P_Pic1, Product_Pictures.P_Pic2, Product_Pictures.P_Pic3, Product_Pictures.P_Pic4 FROM Product_Main INNER JOIN Product_Pictures ON Product_Main.P_Pic_ID=Product_Pictures.ID WHERE Product_Main.ID = %d;", $product_id);

      $rows = $db->query($sql);

      $p_pic1 = NULL;
      $p_pic2 = NULL;
      $p_pic3 = NULL;
      $p_pic4 = NULL;

      foreach ($rows as $row) {
        $p_name =  $row["P_Name"];
        $p_price = $row["P_Price"];
        $p_price = number_format( $p_price, 2, ',', '.');
        $p_desc = $row["P_Description"];
        $p_stock =  $row["P_Stock"];
        if($row["P_Pic1"]){
          $p_pic1 =  "/img/product_pics/" . $row["P_Pic1"];
        }
        if($row["P_Pic2"]){
          $p_pic2 =  "/img/product_pics/" . $row["P_Pic2"];
        }
        if($row["P_Pic3"]){
          $p_pic3 =  "/img/product_pics/" . $row["P_Pic3"];
        }
        if($row["P_Pic4"]){
          $p_pic4 =  "/img/product_pics/" . $row["P_Pic4"];
        }
      }


    } catch (PDOException $ex) {
      ?>
      <p>Sorry, a database error occurred. Please try again later.</p>
      <p>(Error details: <?= $ex->getMessage() ?>)</p>
      <?php
    }

  // disconnect from database
    disconnectFromDB($db);
    ?>


    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>
      <?php
      include 'sidebar.html';
      ?>

      <!-- main column -->
      <div class="col-md-10 col-sm-9 col-xs-12">
        <div class="row">
          <!-- Gallery Column -->
          <div class="col-md-3 col-sm-7 col-xs-12" >
            <div class="row">
              <div id="images-carousel" class="caraousel slide" data-ride="carousel" data-interval="false">
                <!-- carousel items -->
                <div class="carousel-inner main-pic">
                  <div class="item active" data-toggle="modal" data-target=".product-img-zoom1">
                    <img src=<?= $p_pic1 ?>></img>
                  </div>
                  <div class="item" data-toggle="modal" data-target=".product-img-zoom2">
                    <img src=<?= $p_pic2 ?>></img>
                  </div>
                  <div class="item" data-toggle="modal" data-target=".product-img-zoom3">
                    <img src=<?= $p_pic3 ?>></img>
                  </div>
                  <div class="item" data-toggle="modal" data-target=".product-img-zoom4">
                    <img src=<?= $p_pic4 ?>></img>
                  </div>
                </div>

                <div class="carousel-nav">
                  <ol class="carousel-indicators">
                    <li data-target="#images-carousel" data-slide-to="0" class="active">
                      <img src=<?= $p_pic1 ?> data-target=<?= $p_pic1 ?>></img>
                    </li>
                    <?php
                    if($p_pic2 != NULL){
                      ?>
                      <li data-target="#images-carousel" data-slide-to="1">
                        <img src=<?= $p_pic2 ?> data-target=<?= $p_pic2 ?>></img>
                      </li>
                      <?php
                    }
                    if($p_pic3 != NULL){
                      ?>
                      <li data-target="#images-carousel" data-slide-to="2">
                        <img src=<?= $p_pic3 ?> data-target=<?= $p_pic3 ?>></img>
                      </li>
                      <?php 
                    }
                    if($p_pic4 != NULL){
                      ?>
                      <li data-target="#images-carousel" data-slide-to="3">
                        <img src=<?= $p_pic4 ?> data-target=<?= $p_pic4 ?>></img>
                      </li>
                      <?php
                    }
                    ?>
                  </ol>
                </div>

                <!-- Modal Window for product image zoom -->
                <div class="modal fade product-img-zoom1" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <img class="product-img-zoom" src=<?= $p_pic1 ?>></img>
                    </div>
                  </div>
                </div>
                <div class="modal fade product-img-zoom2" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <img class="product-img-zoom" src=<?= $p_pic2 ?>></img>
                    </div>
                  </div>
                </div>
                <div class="modal fade product-img-zoom3" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <img class="product-img-zoom" src=<?= $p_pic3 ?>></img>
                    </div>
                  </div>
                </div>
                <div class="modal fade product-img-zoom4" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      </div>
                      <img class="product-img-zoom" src=<?= $p_pic4 ?>></img>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div> <!-- /Gallery Column -->

          <!-- Description Column -->
          <div class="col-md-6 col-xs-12" >

            <p class="section-heading"><?=  $p_name ?></p>
            <div class="row product-descp">
              <p><?=  $p_desc ?></p>

              <ul>
                <li><a href="#">Schematic</a></li>
                <li>Tutorial</li>
                <li>Datasheet</li>
              </ul>
            </div>
          </div> <!-- /Description Column -->

          <!-- Payment Column -->
          <div class="col-md-3 col-sm-5 col-xs-12" >
            <div class="row payment-box">
              <h3>Rp. <?=  $p_price ?></h3>
              <p>Stock: <?=  $p_stock ?></p>
              <h5>Jumlah :</h5>

              <form action="/cart.php" method="get">
                <div>
                  <input type="hidden" name="action" value="add" />
                  <input type="hidden" name="id" value=<?= $product_id ?> />
                  <input type="text" class="form-control quantity-box" name="quantity" placeholder="1" size="5000" required="required" pattern="[0-9]+" title="Please input number only [0-9]"/>
                  <input type="submit" class="estore-btn" value="Add to Cart" />
                </div>
              </form>
            </div>
          </div> <!-- /Payment Column -->

        </div>

        <!-- Comment Row -->
        <div class="row">
          <div class="col-md-9 col-sm-12"> 
            <!-- begin htmlcommentbox.com -->
            <div id="HCB_comment_box"><a href="http://www.htmlcommentbox.com">Widget</a> is loading comments...</div>
            <link rel="stylesheet" type="text/css" href="//www.htmlcommentbox.com/static/skins/bootstrap/twitter-bootstrap.css?v=0" />
            <script type="text/javascript" id="hcb"> /*<!--*/ if(!window.hcb_user){hcb_user={};} (function(){var s=document.createElement("script"), l=(""+window.location).replace(/'/g,"%27") || hcb_user.PAGE, h="//www.htmlcommentbox.com";s.setAttribute("type","text/javascript");s.setAttribute("src", h+"/jread?page="+encodeURIComponent(l).replace("+","%2B")+"&mod=%241%24wq1rdBcg%24FT6NOg5wrG6L1k%2Fwmni2i0"+"&opts=16862&num=10");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
            <!-- end htmlcommentbox.com -->
          </div>
        </div><!-- /Comment Row -->

      </div><!-- /main column -->


    </div> <!-- /Main Container -->



    <?php
    include 'footer.html';
    ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
