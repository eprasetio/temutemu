/*
estore.js
script to automate html elements' generation
*/

"use strict";

(function(){
  window.onload = createProductTiles;
})();

function createOneProductTile(idx){
  
  
  var ptile = document.createElement("div");
  ptile.className = "tile";

  var plink = document.createElement("a");
  plink.href = "product_page.php";

  ptile.appendChild(plink);

  var pthumb = document.createElement("img");
  pthumb.src = "img/thumb/eeb2.png";

  var ptitle = document.createElement("p");
  ptitle.innerHTML = "Test with JS " + idx;

  var pprice = document.createElement("p");
  pprice.innerHTML = "Rp. 70,000.00";

  var pborder = document.createElement("hr");

  plink.appendChild(pthumb);
  plink.appendChild(ptitle);
  plink.appendChild(pprice);
  plink.appendChild(pborder);

  return ptile;
}

function createProductTiles(){
  var container = document.getElementById("product-tiles");
  for(var i = 0; i < 20; i++){
    container.appendChild(createOneProductTile(i));
  }
}