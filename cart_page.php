<?php  
if (session_id() == "")
  session_start(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="favicon/">

  <title>TemuTemu Cart Page</title>

  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">

  <!-- Get google open sans -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="/css/gaya.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>

    <?php
    $_SESSION['curr-page']='cart';
    include 'header.php';
    ?>

    <!-- Main Container -->
    <div class="container main-container">
      <br>
      <br>
      <?php
      include 'sidebar.html';
      ?>

      <!-- Right main content -->
      <div class="col-md-10 col-sm-9 col-xs-12" >

        <!-- 1st Row -->
        <p class="section-heading">Kereta Belanja</p>
        <div class="row cart-container">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>Item Name</th>
                <th>Qty</th>
                <th>Unit Price</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $total_cost = displayCart();
              $p_total_cost = number_format( $total_cost, 2, ',', '.');

              if($_SESSION["cart-quantity"] <= 0){
                $next_page = "#";
                $button_class = "estore-btn-disabled";
              }else{
                $next_page = "/shipping";
                $button_class = "estore-btn";
              }
              ?>
            </tbody>
          </table>

          <dl class="dl-horizontal pull-right">
            <!--<dt>Sub-total:</dt>
            <dd>$999.99</dd>

            <dt>Shipping Cost:</dt>
            <dd>$0.01</dd>-->

            <dt>Total:</dt>
            <dd>Rp <?= $p_total_cost ?></dd>
          </dl>
          <p class="pull-left">
            <a href="/cart.php?id=1&action=empty&quantity=0"><u><i>Empty Shopping Cart</i></u></a>
          </p>
          <div class="clearfix"></div>

          <a href=<?=  $next_page ?> class="<?= $button_class ?> pull-right">Check out</a>
          <a href="/product_list/0" class="estore-btn">Continue Shopping</a>

        </div> <!-- /1st Row -->

        <br>
        <br>
      </div> <!-- / Right main content -->

      <br>
      <br>
    </div> <!-- / Main Container -->

    <?php
    include 'footer.html';
    ?>

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</body>
</html>
